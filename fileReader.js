const fs = require('fs');

function fileReader(filePath, cb) {
  return fs.readFile(filePath, 'utf8', (_error, data) => {
    cb(null, data);
  });
}

module.exports = fileReader;
