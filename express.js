const express = require('express');
const path = require('path');
const fileReader = require('./fileReader');

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'resourses')));

const port = 3000;

app.get('/:filePath', (req, res) => {
  const { filePath } = req.params;
  const dirPath = './resourses/objects/';
  fileReader(`${dirPath}${filePath}`, (_error, data) => res.json(data));
});
app.get('/', (req, res) => res.render('/resourses/index'));


app.listen(port, () => console.log(`Listening at http://localhost:${port}`));
