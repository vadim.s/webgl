const canvas = document.getElementById('mycanvas');
const gl = canvas.getContext('webgl');
const { mat4, vec3 } = glMatrix; // using 'gl-matrix' library

const filePath = canvas.dataset.search;

fetch(`/${filePath}`)
  .then(response => response.json())
  .then(data => {
    console.log('Data fetched!');
    initDemo(data);
  });

const vertexShaderText = `
 precision mediump float;
 attribute vec3 vertPosition;
 attribute vec3 vertColor;
 varying vec3 fragColor;
 uniform mat4 mWorld;
 uniform mat4 mView;
 uniform mat4 mProj;
  void main() {
    fragColor = vertColor;
    gl_Position = mProj * mView * mWorld * vec4(vertPosition, 1.0);
  }
`;

const fragmentShaderText = `
  precision mediump float;
  varying vec3 fragColor;
  void main() {
    gl_FragColor = vec4(fragColor, 1.0);
  }
`;

let state = {
  ui: {
    dragging: false,
    mouse: {
      lastX: -1,
      lastY: -1,
    },
    pressedKeys: {},
  },
  animation: {},
  app: {
    angle: {
      x: 0,
      y: 0,
    },
    eye : {
      x: 0,
      y: 0,
      z: -10, // -10 in case of cube
    },
  },
};

function initDemo(data) {
  initCallbacks();
  animate(data);
}

function initCallbacks() {
  document.onkeydown = keydown;
  document.onkeyup = keyup;
  canvas.onmousedown = mousedown;
  canvas.onmouseup = mouseup;
  canvas.onmousemove = mousemove;
}

function animate(data) {
  // Parser
  const processed = data.trim().split(/\n/).map(item => item.split(' '));
  const boxVertices = processed.filter(i => i[0] === 'v').map(i => i.slice(1)).flat().map(i => (Number(i)));
  const boxIndices = processed.filter(i => i[0] === 'f').map(i => i.slice(1)).flat().map(i => (Number(i)));

  const move = function() {
    updateState();
    draw(boxVertices, boxIndices);
    requestAnimationFrame(move);
  };
  move(data);
}

function updateState() {
  const speed = 0.2;
  if (state.ui.pressedKeys[37]) {
    state.app.eye.x += speed;
  } else if (state.ui.pressedKeys[39]) {
    state.app.eye.x -= speed;
  } else if (state.ui.pressedKeys[40]) {
    state.app.eye.y += speed;
  } else if (state.ui.pressedKeys[38]) {
    state.app.eye.y -= speed;
  }
}

function draw(boxVertices, boxIndices) {
  console.log('draw func', boxVertices, boxIndices);
  gl.clearColor(0.75, 0.85, 0.8, 1.0);
  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
  gl.enable(gl.DEPTH_TEST);
  gl.enable(gl.CULL_FACE);
  gl.cullFace(gl.BACK);

  const vertexShader = gl.createShader(gl.VERTEX_SHADER);
  const fragmentShader = gl.createShader(gl.FRAGMENT_SHADER);

  gl.shaderSource(vertexShader, vertexShaderText);
  gl.shaderSource(fragmentShader, fragmentShaderText);

  gl.compileShader(vertexShader);
  gl.compileShader(fragmentShader);

  const boxVertexBufferObject = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, boxVertexBufferObject);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(boxVertices), gl.STATIC_DRAW);

  const boxIndexBufferObject = gl.createBuffer();
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, boxIndexBufferObject);
  gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(boxIndices), gl.STATIC_DRAW);

  const program = gl.createProgram();
  gl.attachShader(program, vertexShader);
  gl.attachShader(program, fragmentShader);
  gl.linkProgram(program);

  gl.validateProgram(program);

  const positionAttribLocation = gl.getAttribLocation(program, 'vertPosition');
  const colorAttribLocation = gl.getAttribLocation(program, 'vertColor');
  gl.vertexAttribPointer(
    positionAttribLocation, // attribute location
    3, // number of elements per attribute
    gl.FLOAT, // type of elements
    false,
    6 * Float32Array.BYTES_PER_ELEMENT, // size of individual vertex
    0 // offset from beginning of a single vertex to this attribute
  );

  gl.vertexAttribPointer(
    colorAttribLocation, // attribute location
    3, // number of elements per attribute
    gl.FLOAT, // type of elements
    false,
    6 * Float32Array.BYTES_PER_ELEMENT, // size of individual vertex
    3 * Float32Array.BYTES_PER_ELEMENT,// offset from beginning of a single vertex to this attribute
  );

  gl.enableVertexAttribArray(positionAttribLocation);
  gl.enableVertexAttribArray(colorAttribLocation);

  // Tell which program should be active
  gl.useProgram(program);

  // Get locations of a specific uniform variables which is part of a given WebGLProgram.
  const matWorldUniformLocation = gl.getUniformLocation(program, 'mWorld');
  const matViewUniformLocation = gl.getUniformLocation(program, 'mView');
  const matProjUniformLocation = gl.getUniformLocation(program, 'mProj');

  // Create initial matrices
  const worldMatrix = mat4.create(); // or new Float32Array(16);
  const viewMatrix = mat4.create();
  const projMatrix = mat4.create();

  // Generate a look-at matrix
  const eye = vec3.fromValues(state.app.eye.x, state.app.eye.y, state.app.eye.z);
  const center =  vec3.fromValues(0,0,0);
  const up = vec3.fromValues(0,1,0);
  mat4.lookAt(viewMatrix, eye, center, up);

  // mat4.scale(worldMatrix, worldMatrix, [0.3, 0.3, 0.3]);
  // mat4.translate(worldMatrix, worldMatrix, [-120, -70, 250]);

  // Generate a perspective projection matrix with the given bounds
  const fovy = glMatrix.glMatrix.toRadian(45); // Vertical field of view in radians
  const aspect = canvas.width / canvas.height; // Aspect ratio
  const near = 0.1; // Near bound of the frustum
  const far = 1000.0; // Far bound of the frustum
  mat4.perspective(projMatrix, fovy, aspect, near, far);

  // Specify matrix values for uniform variables
  gl.uniformMatrix4fv(matWorldUniformLocation, false, worldMatrix);
  gl.uniformMatrix4fv(matViewUniformLocation, false, viewMatrix);
  gl.uniformMatrix4fv(matProjUniformLocation, false, projMatrix);

  // Rotation
  mat4.rotateX(worldMatrix, worldMatrix, state.app.angle.x);
  mat4.rotateY(worldMatrix, worldMatrix, state.app.angle.y);

  // Another working variant:
  // const xRotationMatrix = mat4.create();
  // const yRotationMatrix = mat4.create();
  // const identityMatrix = mat4.create();
  // mat4.rotate(yRotationMatrix, identityMatrix, state.app.angle.x, [0, 1, 0]);
  // mat4.rotate(xRotationMatrix, identityMatrix, state.app.angle.y, [1, 0, 0]);
  // mat4.multiply(worldMatrix, xRotationMatrix, yRotationMatrix);

  gl.uniformMatrix4fv(matWorldUniformLocation, false, worldMatrix);

  gl.clearColor(0.75, 0.85, 0.8, 1.0);
  gl.clear(gl.DEPTH_BUFFER_BIT | gl.COLOR_BUFFER_BIT);
  gl.drawElements(gl.TRIANGLES, boxIndices.length, gl.UNSIGNED_SHORT, 0);
}

function keydown(event) {
  state.ui.pressedKeys[event.keyCode] = true;
}

function keyup(event) {
  state.ui.pressedKeys[event.keyCode] = false;
}

function mousedown(event) {
  let x = event.clientX;
  let y = event.clientY;

  const rect = event.target.getBoundingClientRect();
  if(rect.left <= x && x < rect.right && rect.top <= y && y < rect.bottom) {
    state.ui.mouse.lastX = x;
    state.ui.mouse.lastY = y;
    state.ui.dragging = true;
  }
}

function mouseup(event) {
  state.ui.dragging = false;
}

function mousemove(event) {
  let x = event.clientX;
  let y = event.clientY;
  if (state.ui.dragging) {
    const factor = 0.8 / canvas.height;
    const dx = factor * (x - state.ui.mouse.lastX);
    const dy = factor * (y - state.ui.mouse.lastY);
    state.app.angle.x = state.app.angle.x + dy
    state.app.angle.y = state.app.angle.y + dx
  }
  state.ui.mouse.lastX = x;
  state.ui.mouse.lastY = y;
}

window.onload = initDemo;
